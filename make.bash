#!/bin/bash

langs=(
	english
	afrikaans
	czech
	danish
	german
	greek
	french
	croatian
	hungarian
	polish
	russian
	spanish
)

for i in "${langs[@]}"; do
	sed -e "s/language=.*]/language=${i}]/g" scoresheet.tex | pdflatex --jobname "${i}"
	sed -e "s/language=.*]/language=${i}]/g" -e "s/simple{no}/simple{yes}/g" scoresheet.tex | pdflatex --jobname "${i}_simple"
done

mkdir build && mv ./*.pdf build && rm ./*.log ./*.aux
